package fi.muni.PV168.SwingWorkers;

import fi.muni.PV168.Main;
import fi.muni.PV168.Models.LiquidTableModel;
import fi.muni.PV168.project.LiquidStore.backend.Liquid;
import fi.muni.PV168.project.LiquidStore.backend.LiquidManagerImpl;

import javax.sql.DataSource;
import javax.swing.*;
import java.util.List;

/**
 * @author Filip Čekovský (433588)
 * @version 13.05.2017
 */

public class SaveModelWorker extends SwingWorker<Void, Void> {
    private DataSource ds;
    private LiquidTableModel model;
    private List<JButton> buttons;

    public SaveModelWorker(DataSource ds, LiquidTableModel model, List<JButton> buttons) {
        this.ds = ds;
        this.model = model;
        this.buttons = buttons;
    }

    @Override
    protected Void doInBackground() throws Exception {
        LiquidManagerImpl liquidManager = new LiquidManagerImpl();
        liquidManager.setDs(ds);

        List<Liquid> liquids = model.getLiquids();

        for (Liquid liquid:liquids) {
            liquidManager.editLiquid(liquid);
        }
        return null;
    }

    @Override
    protected void done(){
        Main.enableButtons(buttons);
    }
}
