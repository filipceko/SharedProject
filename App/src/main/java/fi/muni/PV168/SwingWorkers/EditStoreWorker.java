package fi.muni.PV168.SwingWorkers;

import fi.muni.PV168.Main;
import fi.muni.PV168.project.LiquidStore.backend.Store;
import fi.muni.PV168.project.LiquidStore.backend.StoreManagerImpl;

import javax.sql.DataSource;
import javax.swing.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author Filip Čekovský (433588)
 * @version 16.05.2017
 */

public class EditStoreWorker extends SwingWorker<Void, Void> {
    private DataSource ds;
    private List<JButton> buttons;
    private String newName;
    private Long id;
    private DefaultComboBoxModel<Store> model;
    private Store toEdit;

    public EditStoreWorker(DataSource ds, List<JButton> buttons, String newName, Long id,
    DefaultComboBoxModel<Store> model) {
        this.ds = ds;
        this.buttons = buttons;
        this.newName = newName;
        this.id = id;
        this.model = model;
    }

    @Override
    protected Void doInBackground() throws Exception {
        StoreManagerImpl manager = new StoreManagerImpl();
        manager.setDs(ds);

        toEdit = manager.getStoreById(id);
        toEdit.setName(newName);

        manager.editStore(toEdit, toEdit);

        return null;
    }

    @Override
    protected void done(){
        try{
            get();
        } catch (InterruptedException|ExecutionException e){
            //TODO
        } finally {
            Main.enableButtons(buttons);
        }

        model.removeElement(model.getSelectedItem());
        model.addElement(toEdit);
        model.setSelectedItem(toEdit);
    }
}
