package fi.muni.PV168.SwingWorkers;

import fi.muni.PV168.Main;
import fi.muni.PV168.project.LiquidStore.backend.*;

import javax.sql.DataSource;
import javax.swing.*;
import java.util.List;

/**
 * @author Filip Čekovský (433588)
 * @version 13.05.2017
 */

public class CreateLiquidWorker extends SwingWorker<Void, Void> {
    private DataSource ds;
    private Liquid liq;
    private Store store;
    private List<JButton> buttons;

    public CreateLiquidWorker(DataSource ds, Liquid liq, Store store, List<JButton>buttons) {
        this.ds = ds;
        this.liq = liq;
        this.store = store;
        this.buttons = buttons;
    }

    @Override
    protected Void doInBackground() throws Exception {
        LiquidManagerImpl liquidManager = new LiquidManagerImpl();
        liquidManager.setDs(ds);
        liquidManager.createLiquid(liq);

        StockManagerImpl stockManager = new StockManagerImpl();
        stockManager.setDs(ds);
        stockManager.storeLiquid(store, liq);

        return null;
    }

    @Override
    protected void done(){
        Main.enableButtons(buttons);
    }
}
