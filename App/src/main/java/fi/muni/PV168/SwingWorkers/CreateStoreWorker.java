package fi.muni.PV168.SwingWorkers;

import fi.muni.PV168.Main;
import fi.muni.PV168.project.LiquidStore.backend.Store;
import fi.muni.PV168.project.LiquidStore.backend.StoreManagerImpl;

import javax.sql.DataSource;
import javax.swing.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author Filip Čekovský (433588)
 * @version 13.05.2017
 */

public class CreateStoreWorker extends SwingWorker<Store,Void> {
    private DataSource ds;
    private Store store;
    private List<JButton> buttons;
    DefaultComboBoxModel<Store> model;

    public CreateStoreWorker(DataSource ds, Store store, DefaultComboBoxModel<Store> model, List<JButton> buttons) {
        this.ds = ds;
        this.store = store;
        this.model = model;
        this.buttons = buttons;
    }

    @Override
    protected Store doInBackground() throws Exception {
        StoreManagerImpl storeManager = new StoreManagerImpl();
        storeManager.setDs(ds);
        storeManager.createStore(store);
        return store;
    }

    @Override
    protected void done(){
        try {
            model.addElement(get());
        } catch (ExecutionException ex) {
            //TODO log
        } catch (InterruptedException ex) {
            //TODO log
        }

        Main.enableButtons(buttons);
    }
}
