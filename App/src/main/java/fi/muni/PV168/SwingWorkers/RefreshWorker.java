package fi.muni.PV168.SwingWorkers;

import fi.muni.PV168.Main;
import fi.muni.PV168.Models.LiquidTableModel;
import fi.muni.PV168.project.LiquidStore.backend.Liquid;
import fi.muni.PV168.project.LiquidStore.backend.Store;
import fi.muni.PV168.project.LiquidStore.backend.StoreManagerImpl;

import javax.sql.DataSource;
import javax.swing.*;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * @author Filip Čekovský (433588)
 * @version 13.05.2017
 */

public class RefreshWorker extends SwingWorker<Store, Void> {
    protected DataSource ds;
    protected Store store;
    protected LiquidTableModel tableModel;
    protected List<JButton> buttons;

    public RefreshWorker(DataSource ds, Store store, LiquidTableModel tableModel, List<JButton> buttons) {
        this.ds = ds;
        this.store = store;
        this.tableModel = tableModel;
        this.buttons = buttons;
    }

    @Override
    protected Store doInBackground() throws Exception {
        StoreManagerImpl storeManager = new StoreManagerImpl();
        storeManager.setDs(ds);
        return storeManager.getStoreById(store.getId());
    }

    @Override
    protected void done(){
        try {
            Store store = get();
            Set<Liquid> set = store.getInventory().getLiquids();
            tableModel.setLiquids(set);
            Main.enableButtons(buttons);
        } catch (InterruptedException|ExecutionException e){
            //TODO log
        }

    }
}
