package fi.muni.PV168;

import fi.muni.PV168.Frames.MainScreen;
import fi.muni.PV168.project.LiquidStore.backend.Administrator;

import javax.sql.DataSource;
import javax.swing.*;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Filip Čekovský (433588)
 * @version 12.05.2017
 */

public class Main {
    public static void main(String[] args){
        final DataSource ds;
        try {
            ds = Administrator.prepareDataSource();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    MainScreen home = new MainScreen(ds);
                    home.boot();
                }
            });
        } catch (SQLException e){
            //TODO Log
            System.exit(2);
        }
    }

    public static void close(DataSource ds){
        try{
            Administrator.closeDataSource(ds);
        } catch (SQLException e){
            //TODO Log
        }
    }

    public static void disableButtons(List<JButton> buttons){
        if (buttons == null) return;
        for (JButton button : buttons) {
            button.setEnabled(false);
        }
    }

    public static void enableButtons(List<JButton> buttons){
        if (buttons == null) return;
        for (JButton button : buttons) {
            button.setEnabled(true);
        }
    }
}
