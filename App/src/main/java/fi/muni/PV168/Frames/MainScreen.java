package fi.muni.PV168.Frames;

import fi.muni.PV168.Localizer;
import fi.muni.PV168.Main;
import fi.muni.PV168.Models.LiquidTableModel;
import fi.muni.PV168.SwingWorkers.*;
import fi.muni.PV168.project.LiquidStore.backend.Liquid;
import fi.muni.PV168.project.LiquidStore.backend.Store;

import javax.sql.DataSource;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.*;
import java.util.List;

/**
 * @author Filip Čekovský (433588)
 * @version 12.05.2017
 */

public class MainScreen {

    private JPanel headPanel;

    private JTextField nameTextField;
    private JTextField brandTextField;
    private JTextField flavourTextField;
    private JSpinner nicotineSpinner;
    private JSpinner priceSpinner;
    private JSpinner volumeSpinner;
    private JSpinner countSpinner;
    private JComboBox createComboBox;

    private JLabel messageLabelLiq;
    private JButton saveButtonLiq;


    private JComboBox tableComboBox;
    private JTable table1;

    private JButton refreshButton;
    private JButton saveButton;
    private JButton exportButton;
    private JButton editStoreButton;

    private JTextField storeRenameField;
    private JLabel IDLabel;
    private JButton deleteButtonStore;
    private JButton saveButtonStore;
    private JButton backButtonStore;


    private JTextField storeNameField;
    private JButton createButtonCreate;

    private JLabel nameLabelStore;
    private JLabel headingLabelStore;
    private JLabel headingLabelCreate;
    private JLabel nameLabelCreate;
    private JLabel nameLabelLiq;
    private JLabel brandLabelLiq;
    private JLabel flavourLabelLiq;
    private JLabel nicLabelLiq;
    private JLabel priceLabelLiq;
    private JLabel volumeLabelLiq;
    private JLabel countLabelLiq;
    private JLabel storeLabelLiq;
    private JLabel headingLabelLiq;
    private JLabel errorLabel;
    private JLabel errorLabelCreate;
    private JLabel errorLabelStore;

    private LiquidTableModel tableModel;
    private DefaultComboBoxModel<Store> dropdownModel;

    private final DataSource ds;
    private java.util.List<JButton> disablingButtons;
    private Map<String, String> texts;

    public MainScreen(DataSource ds){
        this.ds = ds;
        disablingButtons = new LinkedList<>(
                Arrays.asList(refreshButton, saveButtonLiq, saveButton,
                        exportButton, deleteButtonStore, saveButtonStore, createButtonCreate));
        texts = Localizer.getLocalStrings();
    }

    public void boot(){
        JFrame frame = new JFrame("HomeScreen");
        frame.setJMenuBar(createMenu());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dropdownModel = new DefaultComboBoxModel<>();

        setTexts();

        initTablePage();
        initCreateLiquid();
        initCreateStore();
        initEditStore();

        frame.add(headPanel);
        frame.pack();
        frame.setVisible(true);
    }

    private boolean isNameEmpty(String name, JLabel errorLabel){
        if (name.isEmpty()){
            errorLabel.setText(texts.get("emptyStoreName"));
            errorLabel.setVisible(true);
            Main.enableButtons(disablingButtons);
            return true;
        }
        return false;
    }

    private void initCreateStore(){
        errorLabelCreate.setVisible(false);
        createButtonCreate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Main.disableButtons(disablingButtons);

                String name = storeNameField.getText();
                if (isNameEmpty(name, errorLabelCreate)){
                    return;
                }

                errorLabelCreate.setVisible(false);

                Store newStore = new Store();
                newStore.setName(name);
                CreateStoreWorker worker = new CreateStoreWorker(ds, newStore, dropdownModel, disablingButtons);
                worker.execute();
                storeNameField.setText("");
            }
        });
    }

    private void initCreateLiquid(){
        messageLabelLiq.setVisible(false);

        SpinnerNumberModel priceModel = new SpinnerNumberModel(0.0, 0.0, 100000.0, 0.01);
        SpinnerNumberModel volumeModel = new SpinnerNumberModel(0.0, 0.0, 1000.0, 0.01);
        SpinnerNumberModel nicotineModel = new SpinnerNumberModel(0, 0, 100, 1);
        final SpinnerNumberModel countModel = new SpinnerNumberModel(0, 0, 1000000000, 1);

        priceSpinner.setModel(priceModel);
        volumeSpinner.setModel(volumeModel);
        nicotineSpinner.setModel(nicotineModel);
        countSpinner.setModel(countModel);

        createComboBox.setModel(dropdownModel);

        saveButtonLiq.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Main.disableButtons(disablingButtons);
                Store store = (Store) createComboBox.getSelectedItem();
                if (store == null) {
                    messageLabelLiq.setText(texts.get("validStore")); //ValidStore

                    messageLabelLiq.setVisible(true);
                    Main.enableButtons(disablingButtons);
                } else {
                    Liquid liq = new Liquid();

                    liq.setName(nameTextField.getText());
                    liq.setBrand(brandTextField.getText());
                    liq.setFlavour(flavourTextField.getText());
                    liq.setNicotineStrength((int)nicotineSpinner.getValue());
                    liq.setPrice(new BigDecimal((Double)priceSpinner.getValue()));
                    liq.setVolume(new BigDecimal((Double)volumeSpinner.getValue()));
                    liq.setCount((int)countSpinner.getValue());

                    String error = validateLiquid(liq);
                    if (error != null && !error.isEmpty()){
                        messageLabelLiq.setText(error);
                        messageLabelLiq.setVisible(true);
                        Main.enableButtons(disablingButtons);
                    } else {
                        CreateLiquidWorker worker = new CreateLiquidWorker(ds, liq, store, disablingButtons);
                        worker.execute();

                        nameTextField.setText("");
                        brandTextField.setText("");
                        flavourTextField.setText("");
                        nicotineSpinner.setValue(0);
                        priceSpinner.setValue(0.00);
                        volumeSpinner.setValue(0.00);
                        countSpinner.setValue(0);
                    }
                }
            }
        });
    }

    private void initTablePage(){
        editStoreButton.setEnabled(false);
        errorLabel.setVisible(false);

        tableModel = new LiquidTableModel(texts);
        table1.setModel(tableModel);
        tableComboBox.setModel(dropdownModel);

        ActionListener refreshListener =  new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Main.disableButtons(disablingButtons);
                errorLabel.setVisible(false);
                Store store = (Store) tableComboBox.getSelectedItem();
                if (store == null) {
                    Main.enableButtons(disablingButtons);
                    editStoreButton.setEnabled(false);
                    return;
                } else {
                    editStoreButton.setEnabled(true);
                }
                RefreshWorker worker = new RefreshWorker(ds, store, tableModel, disablingButtons);
                worker.execute();
            }
        };

        tableComboBox.addActionListener(refreshListener);

        refreshButton.addActionListener(refreshListener);

        exportButton.addActionListener(refreshListener);

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Main.disableButtons(disablingButtons);

                List<Liquid> liquids = tableModel.getLiquids();

                for (Liquid liquid:liquids) {
                    String message = validateLiquid(liquid);
                    if (!message.isEmpty()){
                        message += "ID :" + liquid.getId() + "\n";
                        errorLabel.setText(message);
                        errorLabel.setVisible(true);
                        return;
                    }
                }

                SaveModelWorker worker = new SaveModelWorker(ds, tableModel, disablingButtons);
                worker.execute();
            }
        });

        exportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Main.disableButtons(disablingButtons);
                Map<Long, Integer> seleceted =tableModel.getSelected();
                java.util.List<Liquid> liquids = tableModel.getLiquids();
                for (Liquid liq : liquids) {
                    Integer count = liq.getCount() - seleceted.get(liq.getId());
                    if (count < 0){
                        Main.enableButtons(disablingButtons);
                        JOptionPane.showMessageDialog(headPanel,
                                texts.get("resources"));
                        return;
                    }
                }

                for (Liquid liq:liquids){
                    Integer count = liq.getCount() - seleceted.get(liq.getId());
                    liq.setCount(count);
                }

                SaveModelWorker worker = new SaveModelWorker(ds, tableModel, disablingButtons);
                worker.execute();
            }
        });

        editStoreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Store store = (Store) tableComboBox.getSelectedItem();
                storeRenameField.setText(store.getName());
                IDLabel.setText("ID: " + store.getId());
                CardLayout layout = (CardLayout) headPanel.getLayout();
                layout.show(headPanel, "Card4");
            }
        });
    }

    private void initEditStore(){
        errorLabelStore.setVisible(false);
        saveButtonStore.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Main.disableButtons(disablingButtons);
                Long id = new Long(IDLabel.getText().substring(4));
                String name = storeRenameField.getText();

                if (isNameEmpty(name, errorLabelStore)){
                    return;
                }

                errorLabelStore.setVisible(false);

                EditStoreWorker worker = new EditStoreWorker(ds, disablingButtons, name, id, dropdownModel);
                worker.execute();

                CardLayout layout = (CardLayout) headPanel.getLayout();
                layout.show(headPanel, "Card1");
            }
        });

        deleteButtonStore.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Main.disableButtons(disablingButtons);
                Long id = new Long(IDLabel.getText().substring(4));

                DeleteStoreWorker worker = new DeleteStoreWorker(ds, disablingButtons, id, dropdownModel);
                worker.execute();

                CardLayout layout = (CardLayout) headPanel.getLayout();
                layout.show(headPanel, "Card1");
            }
        });

        backButtonStore.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CardLayout layout = (CardLayout) headPanel.getLayout();
                layout.show(headPanel, "Card1");
            }
        });
    }

    private String validateLiquid (Liquid liq){
        String message = "";
        if (liq.getName().isEmpty()){
            message = texts.get("emptyLiqName");
            return message;
        }
        if (liq.getBrand().isEmpty()){
            message = texts.get("emptyLiqBrand");
            return message;
        }
        if (liq.getVolume().compareTo(new BigDecimal(0.001)) == -1){
            message = texts.get("smallLiqVolume");
            return message;
        }
        if (liq.getNicotineStrength() < 0 || liq.getCount() < 0){
            message = texts.get("negativeLiqValues");
            return message;
        }
        if (liq.getPrice().compareTo(new BigDecimal(0)) == -1){
            message = texts.get("negativeLiqPrice") + " ";
            return message;
        }

        return message;
    }

    private JMenuBar createMenu() {
        //root menu
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu(texts.get("fileMenu"));
        JMenu windowMenu  = new JMenu(texts.get("windowMenu"));
        final JMenu helpMenu = new JMenu(texts.get("helpMenu"));

        menuBar.add(fileMenu);
        menuBar.add(windowMenu);
        menuBar.add(Box.createHorizontalGlue());
        menuBar.add(helpMenu);

        //Window menu
        JMenuItem overview = new JMenuItem(texts.get("stock"));
        JMenuItem imp = new JMenuItem(texts.get("import"));
        JMenuItem  store = new JMenuItem(texts.get("createStore"));
        windowMenu.add(overview);
        windowMenu.add(store);
        windowMenu.add(imp);

        overview.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CardLayout layout = (CardLayout) headPanel.getLayout();
                layout.show(headPanel, "Card1");
            }
        });

        imp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CardLayout layout = (CardLayout) headPanel.getLayout();
                messageLabelLiq.setVisible(false);
                layout.show(headPanel, "Card2");
            }
        });

        store.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CardLayout layout = (CardLayout) headPanel.getLayout();
                layout.show(headPanel, "Card3");
            }
        });

        //menu File
        JMenuItem exitMenuItem = new JMenuItem(texts.get("exit"));
        fileMenu.add(exitMenuItem);

        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Main.close(ds);
                System.exit(1);
            }
        });

        //menu Help
        JMenuItem aboutMenuItem = new JMenuItem(texts.get("about"));
        helpMenu.add(aboutMenuItem);

        aboutMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JOptionPane.showMessageDialog(helpMenu, texts.get("aboutText"),
                        texts.get("about"), JOptionPane.INFORMATION_MESSAGE);
            }
        });

        return menuBar;
    }


    private void setTexts(){
        //Labels
        setTextTo("headingLabelLiq", headingLabelLiq);
        setTextTo("nameLabelLiq", nameLabelLiq);
        setTextTo("brandLabelLiq", brandLabelLiq);
        setTextTo("flavourLabelLiq", flavourLabelLiq);
        setTextTo("nicLabelLiq", nicLabelLiq);
        setTextTo("priceLabelLiq", priceLabelLiq);
        setTextTo("volumeLabelLiq", volumeLabelLiq);
        setTextTo("countLabelLiq", countLabelLiq);
        setTextTo("storeLabelLiq", storeLabelLiq);
        setTextTo("headingLabelCreate", headingLabelCreate);
        setTextTo("nameLabelCreate", nameLabelCreate);
        setTextTo("headingLabelStore", headingLabelStore);
        setTextTo("nameLabelStore", nameLabelStore);

        //Buttons
        setTextTo("editStoreButton", editStoreButton);
        setTextTo("refreshButton", refreshButton);
        setTextTo("saveButton", saveButton);
        setTextTo("exportButton", exportButton);
        setTextTo("saveButtonLiq", saveButtonLiq);
        setTextTo("createButtonCreate", createButtonCreate);
        setTextTo("backButtonStore", backButtonStore);
        setTextTo("deleteButtonStore", deleteButtonStore);
        setTextTo("saveButtonStore", saveButtonStore);
    }

    private void setTextTo(String key, JLabel label){
        String text = texts.get(key);
        label.setText(text);
    }

    private void setTextTo(String key, JButton button){
        String text = texts.get(key);
        button.setText(text);
    }
}
