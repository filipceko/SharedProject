package fi.muni.PV168.Models;

import fi.muni.PV168.project.LiquidStore.backend.Liquid;

import javax.swing.table.AbstractTableModel;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Filip Čekovský (433588)
 * @version 12.05.2017
 */

public class LiquidTableModel extends AbstractTableModel {
    private List<Liquid> liquids = new ArrayList<>();
    private Map<Long, Integer> selected = new HashMap<>();
    private Map<String, String> texts;

    public LiquidTableModel(Map<String, String> texts) {
        this.texts = texts;
    }

    public void setLiquids (Set<Liquid> set){
        liquids = new ArrayList<>(set);
        selected = new HashMap<>();
        for (Liquid liq: liquids) {
            selected.put(liq.getId(), 0);
        }
        fireTableDataChanged();
    }
    
    public void resteSelect(){
        Set<Long> keys = selected.keySet();
        for (Long key:keys) {
            selected.put(key,0);
        }
    }

    public Map<Long, Integer> getSelected(){
        return selected;
    }

    public List<Liquid> getLiquids() {
        return liquids;
    }

    @Override
    public int getRowCount() {
        return liquids.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Long.class;
            case 1:
            case 2:
            case 3:
                return String.class;
            case 4:
            case 7:
            case 8:
                return Integer.class;
            case 5:
            case 6:
                return BigDecimal.class;
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        Liquid liq = liquids.get(row);
        switch (column){
            case 0:
                return liq.getId();
            case 1:
                return liq.getName();
            case 2:
                return liq.getBrand();
            case 3:
                return liq.getFlavour();
            case 4:
                return liq.getNicotineStrength();
            case 5:
                return liq.getPrice().setScale(2, BigDecimal.ROUND_HALF_UP);
            case 6:
                return liq.getVolume().setScale(2, BigDecimal.ROUND_HALF_UP);
            case 7:
                return liq.getCount();
            case 8:
                return selected.get(liq.getId());
            default:
                throw new IllegalArgumentException("Column index out of range");
        }
    }

    @Override
    public String getColumnName(int column){
        switch (column){
            case 0:
                return texts.get("idTable");
            case 1:
                return texts.get("nameTable");
            case 2:
                return texts.get("brandTable");
            case 3:
                return texts.get("flavourTable");
            case 4:
                return texts.get("nicTable");
            case 5:
                return texts.get("priceTable");
            case 6:
                return texts.get("volumeTable");
            case 7:
                return texts.get("storedTable");
            case 8:
                return texts.get("selectTable");
            default:
                throw new IllegalArgumentException("Column index out of range");
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Liquid liq = liquids.get(rowIndex);
        switch (columnIndex) {
            case 0:
                liq.setId((Long) aValue);
                break;
            case 1:
                liq.setName((String) aValue);
                break;
            case 2:
                liq.setBrand((String) aValue);
                break;
            case 3:
                liq.setFlavour((String) aValue);
                break;
            case 4:
                liq.setNicotineStrength((Integer) aValue);
                break;
            case 5:
                liq.setPrice((BigDecimal) aValue);
                break;
            case 6:
                liq.setVolume((BigDecimal) aValue);
                break;
            case 7:
                liq.setCount((Integer) aValue);
                break;
            case 8:
                selected.put(liq.getId(), (Integer) aValue);
                break;
            default:
                throw new IllegalArgumentException("columnIndex out of range");
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return false;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                return true;
            default:
                throw new IllegalArgumentException("columnIndex out of range");
        }
    }
}
