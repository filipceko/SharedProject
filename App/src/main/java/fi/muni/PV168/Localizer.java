package fi.muni.PV168;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author Filip Čekovský (433588)
 * @version 16.05.2017
 */

public class Localizer {

    public static Map<String, String> getLocalStrings(){
        Map<String, String> strings = new HashMap<>();
        String language = Locale.getDefault().getDisplayLanguage();

        switch (language){
            case "Deutch":
                strings.put("fileMenu","Súbor");
                strings.put("exit","Zavrieť");
                strings.put("windowMenu","Zobraziť");
                strings.put("stock","Prehľad");
                strings.put("createStore","Vytvorť Sklad");
                strings.put("import","Naskladniť Liquid");
                strings.put("helpMenu","Pomoc");
                strings.put("about","O projekte");
                strings.put("aboutText","Liquid Store Manager by Čeko X)");

                //Table Page
                strings.put("editStoreButton","Upraviť Sklad");
                strings.put("refreshButton","Onoviť");
                strings.put("saveButton","Uložiť");
                strings.put("exportButton","Vyskladniť");

                //Liquid Page
                strings.put("headingLabelLiq","Vytovor Liquid");
                strings.put("nameLabelLiq","Meno:");
                strings.put("brandLabelLiq","Značka:");
                strings.put("flavourLabelLiq","Príchuť:");
                strings.put("nicLabelLiq","Sila Nikotínu:");
                strings.put("priceLabelLiq","Cena:");
                strings.put("volumeLabelLiq","Objem:");
                strings.put("countLabelLiq","Počet:");
                strings.put("storeLabelLiq","Na Sklad:");
                strings.put("saveButtonLiq","Uložiť");

                //Create Store Page
                strings.put("nameLabelCreate","Meno:");
                strings.put("headingLabelCreate","Vytvor Sklad:");
                strings.put("createButtonCreate","Vytvoriť");

                //Edit Store Page
                strings.put("headingLabelStore","Uprav Sklad:");
                strings.put("nameLabelStore","Meno:");
                strings.put("saveButtonStore","Uložiť");
                strings.put("backButtonStore", "Späť");
                strings.put("deleteButtonStore", "Zmazať Sklad");
                strings.put("saveButtonStore", "UloŽiť Zmeny");


                //Errors
                strings.put("validStore","Musíťe si zvoliť sklad");
                strings.put("resources", "Nie je naskladnených dostatok Liquidov");
                strings.put("emptyStoreName", "Názov skladu musí byť zadaný");
                strings.put("emptyLiqName", "Prosím zadajte názov Liquidu");
                strings.put("emptyLiqBrand", "Prosím zadajte značku Liquidu");
                strings.put("smallLiqVolume", "Objem liquidu musí byť aspoň 0.001 ml");
                strings.put("negativeLiqValues", "Sila nikotínu a počet naskladňovaných liquidov musí byť kladný");
                strings.put("negativeLiqPrice","Cena Liquidov musí byť pozitívna");
                strings.put("", "");

                strings.put("idTable","ID");
                strings.put("nameTable","Meno");
                strings.put("brandTable","Značka");
                strings.put("flavourTable","Príchuť");
                strings.put("nicTable","Nikotín");
                strings.put("priceTable","Cena");
                strings.put("volumeTable","Objem");
                strings.put("storedTable","Uskaladnených");
                strings.put("selectTable","Zvolených");
                break;

            case "Slovenčina":
                //Menu
                strings.put("fileMenu","Súbor");
                strings.put("exit","Zavrieť");
                strings.put("windowMenu","Zobraziť");
                strings.put("stock","Prehľad");
                strings.put("createStore","Vytvorť Sklad");
                strings.put("import","Naskladniť Liquid");
                strings.put("helpMenu","Pomoc");
                strings.put("about","O projekte");
                strings.put("aboutText","Liquid Store Manager by Čeko ;)");

                //Table Page
                strings.put("editStoreButton","Upraviť Sklad");
                strings.put("refreshButton","Onoviť");
                strings.put("saveButton","Uložiť");
                strings.put("exportButton","Vyskladniť");

                //Liquid Page
                strings.put("headingLabelLiq","Vytovor Liquid");
                strings.put("nameLabelLiq","Meno:");
                strings.put("brandLabelLiq","Značka:");
                strings.put("flavourLabelLiq","Príchuť:");
                strings.put("nicLabelLiq","Sila Nikotínu:");
                strings.put("priceLabelLiq","Cena:");
                strings.put("volumeLabelLiq","Objem:");
                strings.put("countLabelLiq","Počet:");
                strings.put("storeLabelLiq","Na Sklad:");
                strings.put("saveButtonLiq","Uložiť");

                //Create Store Page
                strings.put("nameLabelCreate","Meno:");
                strings.put("headingLabelCreate","Vytvor Sklad:");
                strings.put("createButtonCreate","Vytvoriť");

                //Edit Store Page
                strings.put("headingLabelStore","Uprav Sklad:");
                strings.put("nameLabelStore","Meno:");
                strings.put("saveButtonStore","Uložiť");
                strings.put("backButtonStore", "Späť");
                strings.put("deleteButtonStore", "Zmazať Sklad");
                strings.put("saveButtonStore", "UloŽiť Zmeny");


                //Errors
                strings.put("validStore","Musíťe si zvoliť sklad");
                strings.put("resources", "Nie je naskladnených dostatok Liquidov");
                strings.put("emptyStoreName", "Názov skladu musí byť zadaný");
                strings.put("emptyLiqName", "Prosím zadajte názov Liquidu");
                strings.put("emptyLiqBrand", "Prosím zadajte značku Liquidu");
                strings.put("smallLiqVolume", "Objem liquidu musí byť aspoň 0.001 ml");
                strings.put("negativeLiqValues", "Sila nikotínu a počet naskladňovaných liquidov musí byť kladný");
                strings.put("negativeLiqPrice","Cena Liquidov musí byť pozitívna");

                //Table
                strings.put("idTable","ID");
                strings.put("nameTable","Meno");
                strings.put("brandTable","Značka");
                strings.put("flavourTable","Príchuť");
                strings.put("nicTable","Nikotín");
                strings.put("priceTable","Cena");
                strings.put("volumeTable","Objem");
                strings.put("storedTable","Uskaladnených");
                strings.put("selectTable","Zvolených");

                break;

            default :
                //Menu
                strings.put("fileMenu","File");
                strings.put("exit","Exit");
                strings.put("windowMenu","View");
                strings.put("stock","Overview");
                strings.put("createStore","Create Store");
                strings.put("import","Import Liquid");
                strings.put("helpMenu","Help");
                strings.put("about","About");
                strings.put("aboutText","Liquid Store Manager by Čeko :*");

                //Table Page
                strings.put("editStoreButton","Edit Store");
                strings.put("refreshButton","Refresh");
                strings.put("saveButton","Save");
                strings.put("exportButton","Export");

                //Liquid Page
                strings.put("headingLabelLiq","Create a Liquid");
                strings.put("nameLabelLiq","Name:");
                strings.put("brandLabelLiq","Brand:");
                strings.put("flavourLabelLiq","Flavour:");
                strings.put("nicLabelLiq","Nicotine Strength:");
                strings.put("priceLabelLiq","Price:");
                strings.put("volumeLabelLiq","Volume:");
                strings.put("countLabelLiq","Count:");
                strings.put("storeLabelLiq","To Store:");
                strings.put("saveButtonLiq","Save");

                //Create Store Page
                strings.put("nameLabelCreate","Name:");
                strings.put("headingLabelCreate","Create Store:");
                strings.put("createButtonCreate","Create");

                //Edit Store Page
                strings.put("headingLabelStore","Edit Store:");
                strings.put("nameLabelStore","Name:");
                strings.put("backButtonStore", "Back");
                strings.put("deleteButtonStore", "Delete Store");
                strings.put("saveButtonStore", "Save Changes");


                //Errors
                strings.put("validStore","Valid Store must be selected");
                strings.put("resources", "Not enough liquids in store");
                strings.put("emptyStoreName", "Store name should not be empty, please insert a value");
                strings.put("emptyLiqName", "Please insert Liquid's name");
                strings.put("emptyLiqBrand", "Please insert Liquid's brand");
                strings.put("smallLiqVolume", "Volume must be greater than 0.001");
                strings.put("negativeLiqValues", "Nicotine Strength and number of stored Liquids must not be a negative");
                strings.put("negativeLiqPrice","Liquid's price must be positive");
                strings.put("","");

                //Table
                strings.put("idTable","ID");
                strings.put("nameTable","Name");
                strings.put("brandTable","Brand");
                strings.put("flavourTable","Flavour");
                strings.put("nicTable","Nicotine");
                strings.put("priceTable","Price");
                strings.put("volumeTable","Volume");
                strings.put("storedTable","Stored");
                strings.put("selectTable","Selected");
                break;
        }
        return strings;
    }
}
