package fi.muni.PV168.project.LiquidStore;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import javax.sql.DataSource;

import common.DBUtils;
import fi.muni.PV168.project.LiquidStore.backend.*;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Assertions.*;

/**
 * @author Filip Čekovský (433588)
 * @version 20.03.2017
 */

public class StoreManagerImplTest {
    private StoreManagerImpl manager;
    private DataSource ds;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static DataSource prepareDataSource() throws SQLException {
        EmbeddedDataSource ds = new EmbeddedDataSource();
        ds.setDatabaseName("memory:liquidmngr-test");
        ds.setCreateDatabase("create");
        return ds;
    }

    @Before
    public void setUp() throws SQLException {
        ds = prepareDataSource();
        DBUtils.executeSqlScript(ds,Administrator.class.getResource("/createTables.sql"));
        manager = new StoreManagerImpl();
        manager.setDs(ds);
        prepareTestData();
    }

    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(ds,Administrator.class.getResource("/dropTables.sql"));
    }

    //--------------------------------------------------------------------------
    // Preparing test data
    //--------------------------------------------------------------------------
    private Store s1, s2, s3;
    private Liquid liq1;

    private void prepareTestData(){
        s1 = new Store("TestStore1");
        s2 = new Store();
        Set<Liquid> set1 = new HashSet<>();
        s3 = new Store(set1);
        s3.setId(800L);
        liq1 = new Liquid("TestLiquid", "TestBrand",
                21, "Banana", new BigDecimal(4.30));

        LiquidManagerImpl liqManager = new LiquidManagerImpl();
        liqManager.setDs(ds);
        liqManager.createLiquid(liq1);
    }


    @Test
    public void createEmptyStore(){
        manager.createStore(s1);
        assertThat(s1.getId()).isNotNull();
        assertThat(manager.getStoreById(s1.getId())).isNotNull();
        assertThat(manager.getStoreById(s1.getId()).getId()).isEqualTo(s1.getId());
        assertThat(manager.getStoreById(s1.getId()).getInventory().getLiquids()).isEmpty();
    }

    @Test
    public void CreateWithNull(){
        assertThatThrownBy(() -> manager.createStore(null))
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    public void createStoreTwoTimes(){
        manager.createStore(s2);

        Long firstInputId = manager.getStoreById(s2.getId()).getId();

        manager.createStore(s2);

        assertThat(firstInputId).isNotEqualTo(s2.getId());
        assertThat(manager.getStoreById(firstInputId)).isNotNull();
    }

    @Test
    public void createNonemptyStore(){
        Store oneLiquid = new Store();
        oneLiquid.storeLiquid(liq1);
        manager.createStore(oneLiquid);
        assertThat(oneLiquid.getId()).isNotNull();

        Store fromDb = manager.getStoreById(oneLiquid.getId());
        assertThat(fromDb).isNotNull();
        assertThat(fromDb.getId()).isEqualTo(oneLiquid.getId());
        assertThat(fromDb.getInventory().getLiquids()).contains(liq1);
    }


    @Test
    public void createStoreWitId(){
        manager.createStore(s3);

        assertThat(s3.getId()).isNotEqualTo(800L);
    }

    @Test
    public void getStoreById(){
        manager.createStore(s1);
        assertThat(manager.getStoreById(s1.getId()).getId()).isEqualTo(s1.getId());
        assertThat(manager.getStoreById(s1.getId()).getInventory().getLiquids()).isEmpty();

    }

    @Test
    public void getStoreByIdeWithLiquid(){
        Store oneLiquid = new Store();
        oneLiquid.storeLiquid(liq1);
        manager.createStore(oneLiquid);
        Store fromDb = manager.getStoreById(oneLiquid.getId());

        assertThat(fromDb.getInventory().getLiquids()).contains(liq1);
    }

    @Test
    public void getStoreByNull(){
        assertThat(manager.getStoreById(null)).isNull();
    }

    @Test
    public void getStoreByInvalidId(){
        assertThat(manager.getStoreById(900L)).isNull();
    }

    @Test
    public void deleteStore(){
        manager.createStore(s3);
        Long id = s3.getId();
        manager.deleteStore(s3);

        assertThat(manager.getStoreById(id)).isNull();
        assertThat(s3.getId()).isNull();
    }

    @Test
    public void deleteNull(){
        assertThatThrownBy(() -> manager.deleteStore(null))
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    public void editStore(){
        Store base = new Store();
        Store data = new Store();
        data.setId(70L);
        data.storeLiquid(liq1);

        manager.createStore(base);
        Long id = base.getId();
        manager.editStore(base, data);

        assertThat(base.getInventory().getLiquids().contains(liq1));
        assertThat(base.getId()).isEqualTo(id);

        base = manager.getStoreById(base.getId());

        assertThat(base.getInventory().getLiquids().contains(liq1));
    }

    @Test
    public void editStoreWithNull(){
        Store base = new Store();

        manager.createStore(base);
        Long id = (base.getId());

        expectedException.expect(NullPointerException.class);
        manager.editStore(null, base);

        expectedException.expect(NullPointerException.class);
        manager.editStore(base, null);

        assertThat(base.getId()).isEqualTo(id);
        assertThat(manager.getStoreById(id).getInventory().getLiquids().isEmpty());
    }
}
