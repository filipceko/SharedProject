package fi.muni.PV168.project.LiquidStore;

import java.math.BigDecimal;
import java.sql.SQLException;
import javax.sql.DataSource;

import common.DBUtils;
import common.EntityNotFoundException;
import common.LiquidAlreadyInStoreException;
import common.LiquidNotInStore;
import fi.muni.PV168.project.LiquidStore.backend.*;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Assertions.*;

/**
 * @author Filip Čekovský (433588)
 * @version 23.03.2017
 */

public class StockManagerImplTest {
    private StockManagerImpl manager;
    private DataSource ds;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static DataSource prepareDataSource() throws SQLException {
        EmbeddedDataSource ds = new EmbeddedDataSource();
        ds.setDatabaseName("memory:liquidmngr-test");
        ds.setCreateDatabase("create");
        return ds;
    }

    @Before
    public void setUp() throws SQLException {
        ds = prepareDataSource();
        DBUtils.executeSqlScript(ds,Administrator.class.getResource("/createTables.sql"));
        manager = new StockManagerImpl();
        manager.setDs(ds);
        prepareTestData();
    }

    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(ds,Administrator.class.getResource("/dropTables.sql"));
    }

    //--------------------------------------------------------------------------
    // Preparing test data
    //--------------------------------------------------------------------------

    private Liquid l1, l2, l3, neverStored, l4, notCreatedLiquid;
    private Store s1, s2, notCreatedStore;

    private void prepareTestData(){
        l1 = new Liquid("Aron", "MisovTatko",
                0, "Chilli", new BigDecimal(2)) ;
        l2 = new Liquid("Aron", "WixovTatko",
                0, "Chilli", new BigDecimal(2)) ;
        l3 = new Liquid("Aron", "ZimovTatko",
                0, "Chilli", new BigDecimal(2)) ;
        neverStored = new Liquid("Aron", "JurovTatko",
                0, "Chilli", new BigDecimal(2)) ;
        l4 = new Liquid("Aron", "SlonovTatko",
                0, "Chilli", new BigDecimal(2)) ;
        notCreatedLiquid = new Liquid("Aron", "MojTatko",
                0, "Chilli", new BigDecimal(2)) ;
        s1 = new Store();
        s2 = new Store();
        notCreatedStore = new Store();

        LiquidManagerImpl lm = new LiquidManagerImpl();
        lm.setDs(ds);
        lm.createLiquid(l1);
        lm.createLiquid(l2);
        lm.createLiquid(l3);
        lm.createLiquid(l4);
        lm.createLiquid(neverStored);

        StoreManagerImpl sm = new StoreManagerImpl();
        sm.setDs(ds);
        sm.setDs(ds);
        sm.createStore(s1);
        sm.createStore(s2);

    }

    //--------------------------------------------------------------------------
    // Set of tests
    //--------------------------------------------------------------------------

    @Test
    public void storeLiquid(){
        manager.storeLiquid(s1, l1);

        assertThat(s1.contains(l1));
        assertThat(s1.getInventory().findLiquidById(l1.getId()))
                .isEqualToComparingFieldByField(l1);
    }

    @Test(expected = EntityNotFoundException.class)
    public void storeNotCreatedLiquid() {
        manager.storeLiquid(s1, notCreatedLiquid);
    }

    @Test(expected = EntityNotFoundException.class)
    public void storeNotCreatedStore(){
        manager.storeLiquid(notCreatedStore, l1);
    }

    @Test(expected = NullPointerException.class)
    public void storeNullLiquid() {
        manager.storeLiquid(s1, null);
    }

    @Test(expected = NullPointerException.class)
    public void storeNullStore() {
            manager.storeLiquid(null, l1);
    }

    @Test(expected = LiquidAlreadyInStoreException.class)
    public void storeStored() {
        manager.storeLiquid(s2, l2);
        manager.storeLiquid(s1, l2);
    }

    @Test
    public void findStoreOfLiquid(){
        manager.storeLiquid(s2, l3);

        Store fromDb = manager.findStoreOfLiquid(l3);
        assertThat(fromDb.getId()).isEqualTo(s2.getId());
        assertThat(fromDb.contains(l3));
        assertThat(fromDb.getName()).isEqualTo(s2.getName());
    }

    @Test
    public void findStoreOfNotStored(){
        assertThat(manager.findStoreOfLiquid(neverStored)).isNull();
    }

    @Test(expected = NullPointerException.class)
    public void findStoreOfNull(){
        Liquid nullLiq = null;
        manager.findStoreOfLiquid(nullLiq);
    }

    @Test
    public void findStoreByIdOfLiquid(){
        manager.storeLiquid(s2, l3);

        Store fromDb = manager.findStoreOfLiquid(l3.getId());
        assertThat(fromDb.getId()).isEqualTo(s2.getId());
        assertThat(fromDb.contains(l3));
        assertThat(fromDb.getName()).isEqualTo(s2.getName());
    }

    @Test
    public void findStoreByIdOfNotStored(){
        assertThat(manager.findStoreOfLiquid(neverStored.getId())).isNull();
    }

    @Test(expected = NullPointerException.class)
    public void findStoreByIdOfNull(){
        Long nullId = null;
        manager.findStoreOfLiquid(nullId);
    }

    @Test
    public void exportLiquidFromStore(){
        manager.storeLiquid(s1,l4);
        manager.exportLiquidFromStore(s1,l4);
        assertThat(manager.findStoreOfLiquid(l4)).isNull();
    }

    @Test(expected = LiquidNotInStore.class)
    public void exportLiquidFromWrongStore(){
        manager.storeLiquid(s1,l4);
        manager.exportLiquidFromStore(s2,l4);
    }

    @Test(expected = NullPointerException.class)
    public void exportNullFromStore(){
        manager.exportLiquidFromStore(null,l4);
    }

    @Test(expected = NullPointerException.class)
    public void exportLiquidFromNull(){
        manager.exportLiquidFromStore(s1,null);
    }
}
