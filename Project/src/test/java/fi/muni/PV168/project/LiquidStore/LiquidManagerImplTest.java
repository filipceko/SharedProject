package fi.muni.PV168.project.LiquidStore;

import java.math.BigDecimal;
import java.sql.SQLException;
import javax.sql.DataSource;

import common.DBUtils;
import fi.muni.PV168.project.LiquidStore.backend.Liquid;
import fi.muni.PV168.project.LiquidStore.backend.LiquidManagerImpl;
import fi.muni.PV168.project.LiquidStore.backend.Administrator;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Assertions.*;
/**
 * @author Filip Čekovský (433588)
 * @version 10.03.2017
 */

public class LiquidManagerImplTest {
    private LiquidManagerImpl manager;
    private DataSource ds;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static DataSource prepareDataSource() throws SQLException {
        EmbeddedDataSource ds = new EmbeddedDataSource();
        ds.setDatabaseName("memory:liquidmngr-test");
        ds.setCreateDatabase("create");
        return ds;
    }

    @Before
    public void setUp() throws SQLException {
        ds = prepareDataSource();
        DBUtils.executeSqlScript(ds,Administrator.class.getResource("/createTables.sql"));
        manager = new LiquidManagerImpl();
        manager.setDs(ds);
        prepareTestData();
    }

    @After
    public void tearDown() throws SQLException {
        DBUtils.executeSqlScript(ds,Administrator.class.getResource("/dropTables.sql"));
    }

    //--------------------------------------------------------------------------
    // Preparing test data
    //--------------------------------------------------------------------------

    private Liquid l1, l2, l3;

    private void prepareTestData(){
        l1 = new Liquid("Test1", "Brand1",
                12, "Malina", new BigDecimal(4.20));
        l2 = new Liquid("Test2", "Brand1",
                9, "Malina", new BigDecimal(4.50));
        l3 = new Liquid("Test3", "Brand2",
                6, "Mango", new BigDecimal(5.20));

        l2.setId(500L);
    }

    //--------------------------------------------------------------------------
    // Set of tests
    //--------------------------------------------------------------------------

    @Test
    public void createLiquid(){
        manager.createLiquid(l1);
        assertThat(l1.getId()).isNotNull();
        Liquid fromDb = manager.getLiquidById(l1.getId());
        assertThat(fromDb.getId()).isEqualTo(l1.getId());
        assertThat(fromDb).isEqualToComparingFieldByField(l1);

    }

    @Test
    public void CreateWithNull(){
        assertThatThrownBy(() -> manager.createLiquid(null))
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    public void createLiquidTwoTimes(){
        manager.createLiquid(l2);

        Long firstInputId = manager.getLiquidById(l2.getId()).getId();

        manager.createLiquid(l2);

        assertThat(firstInputId).isNotEqualTo(l2.getId());
        assertThat(manager.getLiquidById(firstInputId)).isNotNull();
    }

    @Test
    public void createLiquidWitId(){
        manager.createLiquid(l2);

        assertThat(l2.getId()).isNotEqualTo(500L);
    }

    @Test
    public void getLiquidById(){
        manager.createLiquid(l1);
        assertThat(manager.getLiquidById(l1.getId()))
                .isEqualToComparingFieldByField(l1);
    }

    @Test
    public void getLiquidByNull(){
        assertThat(manager.getLiquidById(null)).isNull();
    }

    @Test
    public void getLiquidByInvalidId(){
        assertThat(manager.getLiquidById(700L)).isNull();
    }

    @Test
    public void deleteLiquid(){
        manager.createLiquid(l3);
        Long id = l3.getId();
        manager.deleteLiquid(id);

        assertThat(manager.getLiquidById(id)).isNull();
    }

    @Test
    public void deleteNull(){
        assertThatThrownBy(() -> manager.deleteLiquid(null))
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    public void editLiquid(){
        Liquid base = new Liquid("Test2", "Brand1",
                9, "Malina", new BigDecimal(4.50));
        Liquid data = new Liquid("Test3", "Brand2",
                6, "Mango", new BigDecimal(5.20));
        data.setId(69L);

        manager.createLiquid(base);
        Long id = base.getId();
        manager.editLiquid(base.getId(), data);

        base = manager.getLiquidById(base.getId());

        assertThat(base).isEqualToComparingOnlyGivenFields(
                data, "name", "brand","nicotineStrength", "flavour", "price");
        assertThat(base.getId()).isEqualTo(id);


    }

    @Test
    public void editLiquidWithNull(){
        Liquid data = new Liquid("Test3", "Brand2",
                6, "Mango", new BigDecimal(5.20));
        Liquid template = new Liquid("Test3", "Brand2",
                6, "Mango", new BigDecimal(5.20));

        manager.createLiquid(data);
        template.setId(data.getId());

        expectedException.expect(NullPointerException.class);
        manager.editLiquid(null, data);

        expectedException.expect(NullPointerException.class);
        manager.editLiquid(data.getId(), null);

        assertThat(data).isEqualToComparingFieldByField(template);
        assertThat(manager.getLiquidById(data.getId())).isEqualToComparingFieldByField(template);
    }
}
