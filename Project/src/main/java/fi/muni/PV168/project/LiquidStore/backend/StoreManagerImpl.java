package fi.muni.PV168.project.LiquidStore.backend;

import common.DBUtils;
import common.ServiceFailureException;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Filip Čekovský (433588)
 * @version 17.03.2017
 */

public class StoreManagerImpl implements StoreManager {

    private DataSource ds = null;

    private void checkDataSource(){
        if (ds == null) throw new IllegalStateException("datasource wasn't set yet");
    }

    @Override
    public void createStore(Store store) throws ServiceFailureException {
        checkDataSource();

        if (store == null) throw new NullPointerException(
                "Input for method createStore is null");

        store.setId(null);

        try (Connection con = ds.getConnection()){
            try (PreparedStatement st = con.prepareStatement("INSERT INTO \"STORE\" (\"NAME\") VALUES (?)",
                    PreparedStatement.RETURN_GENERATED_KEYS)){

                st.setString(1, store.getName());

                st.executeUpdate();

                ResultSet rs = st.getGeneratedKeys();
                if (rs.next()){
                    store.setId(rs.getLong(1));
                }
            }

        } catch (SQLException e){
            throw new ServiceFailureException("Error while creating Store", e);
        }

        StockManagerImpl stockManager = new StockManagerImpl();
        stockManager.setDs(ds);

        for (Liquid liq:store.getInventory().getLiquids()) {
            stockManager.storeLiquid(store,liq);
        }
    }

    @Override
    public void deleteStore(Store store) throws ServiceFailureException {
        checkDataSource();

        if (store == null || store.getId() == null)
            throw new NullPointerException("Attempt to delete null store");

        LiquidManagerImpl liquidManager = new LiquidManagerImpl();
        liquidManager.setDs(ds);

        for (Liquid liq:store.getInventory().getLiquids()) {
            liquidManager.deleteLiquid(liq.getId());
        }

        try (Connection con = ds.getConnection()){
            con.setAutoCommit(false);
            try (PreparedStatement st = con.prepareStatement(
                    "DELETE FROM \"STORE\" WHERE \"ID\" = ?")){

                st.setLong(1, store.getId());

                DBUtils.checkUpdatesCount(st.executeUpdate(), store, false);
                con.commit();
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while deleting store", e);
        }

        store.setId(null);

    }

    @Override
    public void editStore(Store toEdit, Store data) throws ServiceFailureException {
        checkDataSource();

        if (toEdit == null || data == null) throw new NullPointerException(
                "Null passed as input for the editStore method");

        if (toEdit.getId() == null){
            throw new NullPointerException("Passed Store without ID");
        }

        StockManagerImpl stockManager = new StockManagerImpl();
        stockManager.setDs(ds);

        for (Liquid liq:toEdit.getInventory().getLiquids()) {
            if(!data.contains(liq)){
                stockManager.exportLiquidFromStore(toEdit,liq);
            }
        }

        for (Liquid liq:data.getInventory().getLiquids()) {
            if (!toEdit.contains(liq)) {
                stockManager.storeLiquid(toEdit, liq);
            }
        }

        try (Connection con = ds.getConnection()){
            con.setAutoCommit(false);
            try (PreparedStatement st = con.prepareStatement("UPDATE \"STORE\" SET " +
                    "\"NAME\" = ? WHERE \"ID\" = ? ")){
                st.setString(1, data.getName());
                st.setLong(2, toEdit.getId());

                DBUtils.checkUpdatesCount(st.executeUpdate(), data, false);

                con.commit();
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while editing Store", e);
        }

        toEdit.setName(data.getName());
    }

    public Set<Store> getAllStores() throws ServiceFailureException{
        checkDataSource();
        Set<Store> result = new HashSet<>();
        try (Connection con = ds.getConnection()){
            String name;
            Long id;
            Set<Liquid> inventory;

            try (PreparedStatement st = con.prepareStatement(
                    "SELECT \"ID\",\"NAME\" FROM \"STORE\"")){
                try (ResultSet rs = st.executeQuery()){
                    while(rs.next()) {
                        name = rs.getString("NAME");
                        id = rs.getLong("ID");

                        try (PreparedStatement sta = con.prepareStatement("SELECT * FROM \"LIQUID\" " +
                                "WHERE \"STOREID\" = ?")){
                            sta.setLong(1, id);

                            try (ResultSet res = sta.executeQuery()){
                                inventory = LiquidManagerImpl.parseDbResult(res);

                                result.add(new Store(name, inventory, id));
                            }
                        }
                    }
                }
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while getting Liquid for Stores from DB", e);
        }

        return result;

    }

    @Override
    public Store getStoreById(Long id) throws ServiceFailureException {
        checkDataSource();

        if (id == null) return null;

        Store result;
        try (Connection con = ds.getConnection()){
            String name;
            Set<Liquid> inventory;

            try (PreparedStatement st = con.prepareStatement(
                    "SELECT \"ID\",\"NAME\" FROM \"STORE\" WHERE \"ID\" = ?")){
                st.setLong(1, id);

                try (ResultSet rs = st.executeQuery()){
                    if(rs.next()) {
                        name = rs.getString("NAME");

                        if (rs.next()){
                            throw new ServiceFailureException(
                                    "Integrity error, more Stores with the same id");
                        }
                    } else {
                        return null;
                    }
                }
            }

            try (PreparedStatement st = con.prepareStatement("SELECT * FROM \"LIQUID\" WHERE \"STOREID\" = ?")){

                st.setLong(1, id);

                try (ResultSet rs = st.executeQuery()){
                    inventory = LiquidManagerImpl.parseDbResult(rs);
                    result = new Store(name, inventory);
                    result.setId(id);
                }
            }


        } catch (SQLException e){
            throw new ServiceFailureException("Error while getting Liquid from DB", e);
        }
        return result;
    }

    /**
     * Sets datasource for this manager
     * @param ds a DataSource for this manager
     */
    public void setDs(DataSource ds){
        this.ds = ds;
    }


}
