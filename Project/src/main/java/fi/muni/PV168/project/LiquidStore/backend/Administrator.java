package fi.muni.PV168.project.LiquidStore.backend;


import common.DBUtils;
import org.apache.derby.jdbc.EmbeddedDataSource;


import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author Filip Čekovský (433588)
 * @version 10.03.2017
 */

public class Administrator {

    public static DataSource prepareDataSource() throws SQLException {
        EmbeddedDataSource ds = new EmbeddedDataSource();
        ds.setDatabaseName("memory:liquidmngr-test");
        ds.setCreateDatabase("create");
        DBUtils.executeSqlScript(ds,Administrator.class.getResource("/createTables.sql"));
        return ds;
    }

    public static void closeDataSource(DataSource ds) throws SQLException {
        DBUtils.executeSqlScript(ds,Administrator.class.getResource("/dropTables.sql"));
    }


}
