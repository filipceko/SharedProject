package fi.muni.PV168.project.LiquidStore.backend;

import java.math.BigDecimal;

/**
 * @author Filip Čekovský (433588)
 * @version 10.03.2017
 */

public class Liquid {
    private Long id = null;
    private String name;
    private String brand;
    private int nicotineStrength;
    private String flavour;
    private BigDecimal price;
    private Integer count;
    private BigDecimal volume;

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public int getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getNicotineStrength() {
        return nicotineStrength;
    }

    public void setNicotineStrength(int nicotineStrength) {
        this.nicotineStrength = nicotineStrength;
    }

    public String getFlavour() {
        return flavour;
    }

    public void setFlavour(String flavour) {
        this.flavour = flavour;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Liquid(String name, String brand, int nicotineStrength, String flavour, BigDecimal price) {
        this.name = name;
        this.brand = brand;
        this.nicotineStrength = nicotineStrength;
        this.flavour = flavour;
        this.price = price;
    }

    public Liquid() {
        this.name = null;
        this.brand = null;
        this.nicotineStrength = -1;
        this.flavour = null;
        this.price = null;
    }

     public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Liquid{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Liquid liquid = (Liquid) o;

        if (getNicotineStrength() != liquid.getNicotineStrength()) return false;
        if (!getName().equals(liquid.getName())) return false;
        if (!getBrand().equals(liquid.getBrand())) return false;
        if (getFlavour() != null ? !getFlavour().equals(liquid.getFlavour()) : liquid.getFlavour() != null)
            return false;
        return getVolume().equals(liquid.getVolume());
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }
}
