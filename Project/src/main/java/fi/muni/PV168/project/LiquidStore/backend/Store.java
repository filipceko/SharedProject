package fi.muni.PV168.project.LiquidStore.backend;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Filip Čekovský (433588)
 * @version 10.03.2017
 */

public class Store {
    private Set<Liquid> inventory;
    private String name;
    private Long id = null;

    public Store() {
        inventory = new HashSet<>();
    }
    public Store(String name ) {
        inventory = new HashSet<>();
        this.name = name;
    }

    public Store(Set<Liquid> inventory) {
        if (inventory != null){
            this.inventory = inventory;
        }
        else throw new NullPointerException("inventory parameter in constructor id null");
    }
    public Store(String name, Set<Liquid> inventory) {
        if (inventory != null){
            this.inventory = inventory;
        }
        else throw new NullPointerException("inventory parameter in constructor id null");

        this.name = name;
    }

    public Store(String name, Set<Liquid> inventory, Long id) {
        if (inventory != null){
            this.inventory = inventory;
        }
        else throw new NullPointerException("inventory parameter in constructor id null");

        this.name = name;
        this.id = id;
    }

    public void storeSet(SetOfLiquids set){
        inventory.addAll(set.getLiquids());
    }

    public void exportSet(SetOfLiquids set){
        inventory.removeAll(set.getLiquids());
    }

    public SetOfLiquids getInventory(){
        return new SetOfLiquids(inventory);
    }

    public void storeLiquid(Liquid liq){
        inventory.add(liq);
    }

    public void exportLiquid(Liquid liq){
        inventory.remove(liq);
    }

    public boolean contains (Liquid liq) {
        return inventory.contains(liq);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ID: " + id +
                " - " + name;
    }
}
