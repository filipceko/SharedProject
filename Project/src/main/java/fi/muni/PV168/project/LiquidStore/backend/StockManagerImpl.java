package fi.muni.PV168.project.LiquidStore.backend;

import common.*;

import javax.sql.DataSource;
import java.sql.*;

/**
 * @author Filip Čekovský (433588)
 * @version 17.03.2017
 */

public class StockManagerImpl implements StockManager {

    private DataSource ds = null;

    private void checkDataSource(){
        if (ds == null) throw new IllegalStateException("datasource wasn't set yet");
    }

    @Override
    public void storeLiquid(Store store, Liquid liq) throws ServiceFailureException, LiquidAlreadyInStoreException {
        checkDataSource();

        if (liq == null || store == null)
            throw new NullPointerException ("Input for method storeLiquid is null");

        if (liq.getId() == null || store.getId() == null)
            throw new EntityNotFoundException("Liquid or store has id with null value");

        if (findStoreOfLiquid(liq.getId()) != null)
            throw new LiquidAlreadyInStoreException("Liquid is already stored in a Store");

        try (Connection con = ds.getConnection()){
            con.setAutoCommit(false);
            try (PreparedStatement st = con.prepareStatement(
                    "UPDATE \"LIQUID\" SET \"STOREID\" = ? WHERE \"ID\" = ?")){
                st.setLong(1, store.getId());
                st.setLong(2, liq.getId());

                DBUtils.checkUpdatesCount(st.executeUpdate(), liq, false);
                con.commit();
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while storing Liquid", e);
        }

        store.storeLiquid(liq);
    }

    @Override
    public void exportLiquidFromStore(Store store, Liquid liq) throws ServiceFailureException, LiquidNotInStore {
        checkDataSource();

        if (liq == null || store == null)
            throw new NullPointerException ("Input for method export Liquid is null");

        if (liq.getId() == null || store.getId() == null)
            throw new EntityNotFoundException("Liquid or store has id with null value");

        if (!findStoreOfLiquid(liq.getId()).getId().equals(store.getId()))
            throw new LiquidNotInStore("Liquid is not stored in given store");

        try (Connection con = ds.getConnection()){
            con.setAutoCommit(false);
            try (PreparedStatement st = con.prepareStatement(
                    "UPDATE \"LIQUID\" SET \"STOREID\" = ? WHERE \"ID\" = ?")){
                st.setNull(1, Types.BIGINT);
                st.setLong(2, liq.getId());

                DBUtils.checkUpdatesCount(st.executeUpdate(), liq, false);
                con.commit();
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while exporting Liquid", e);
        }

        store.exportLiquid(liq);
    }

    @Override
    public Store findStoreOfLiquid(Liquid liq) throws ServiceFailureException {
        return findStoreOfLiquid(liq.getId());
    }

    @Override
    public Store findStoreOfLiquid(Long id) throws ServiceFailureException {
        checkDataSource();

        if (id == null) throw new NullPointerException("Passed null instead of store id");

        Long storeId;
        try (Connection con = ds.getConnection()){
            try (PreparedStatement st = con.prepareStatement("SELECT \"STOREID\" FROM \"LIQUID\" WHERE \"ID\" = ?")){

                st.setLong(1, id);

                try (ResultSet rs = st.executeQuery()){
                    if(rs.next()) {
                        storeId = rs.getLong("STOREID");
                        if (rs.next()){
                            throw new ServiceFailureException(
                                    "Integrity error, more Liquids with the same id");
                        }
                    } else {
                        return null;
                    }
                }
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while getting Liquid info from DB", e);
        }

        if (storeId == null) return null;

        StoreManagerImpl storeManager = new StoreManagerImpl();
        storeManager.setDs(ds);
        return storeManager.getStoreById(storeId);
    }

    /**
     * Sets datasource for this manager
     * @param ds a DataSource for this manager
     */
    public void setDs(DataSource ds){
        this.ds = ds;
    }
}
