package fi.muni.PV168.project.LiquidStore.backend;

import common.ServiceFailureException;

import java.util.Set;

/**
 * @author Filip Čekovský (433588)
 * @version 10.03.2017
 */

public interface LiquidManager {
    /**
     * Stores new Liquid into the database
     * @param liq to be stored
     * @throws ServiceFailureException if db fails
     */
    void createLiquid(Liquid liq) throws ServiceFailureException;

    /**
     *Deletes Liquid from database
     * @param id of the liquid to be deleted
     * @throws ServiceFailureException id fb fails
     */
    void deleteLiquid(Long id) throws ServiceFailureException;

    /**
     *edits Liquid in the database
     * @param toEdit - Liquid to be edited
     * @param data - Changed liquid
     * @throws ServiceFailureException if db fails
     */
    void editLiquid (Long toEdit, Liquid data) throws ServiceFailureException ;

    /**
     *loads a Liquid from the database
     * @param id of desired Liquid
     * @return Liquid with given id from db, if does not exist null
     * @throws ServiceFailureException if db fails
     */
    Liquid getLiquidById (Long id) throws ServiceFailureException;

    /**
     * Returns all Liquids in the database
     * @return Set of liquids in db
     * @throws ServiceFailureException if db fails
     */
    Set<Liquid> getAllLiquids () throws ServiceFailureException;
}
