package fi.muni.PV168.project.LiquidStore.backend;

import common.DBUtils;
import common.ServiceFailureException;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Filip Čekovský (433588)
 * @version 17.03.2017
 */

public class LiquidManagerImpl implements  LiquidManager{

    private DataSource ds = null;

    private void checkDataSource(){
        if (ds == null) throw new IllegalStateException("datasource wasn't set yet");
    }

    @Override
    public void createLiquid(Liquid liq) throws ServiceFailureException {
        checkDataSource();

        if (liq == null) throw new NullPointerException(
                "Input for method createLiquid is null");

        liq.setId(null);

        Set<Liquid> liquids = getAllLiquids();
        for (Liquid item:liquids){
            if(item.equals(liq)){
                int temp = liq.getCount();
                temp += item.getCount();
                item.setCount(temp);
                editLiquid(item.getId(),item);
                return;
            }
        }

        try (Connection con = ds.getConnection()){
            try (PreparedStatement st = con.prepareStatement("INSERT INTO \"LIQUID\" (\"NAME\"," +
                    "\"BRAND\", \"NIC\", \"FLAVOUR\", \"PRICE\", \"VOLUME\", \"COUNT\") " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?)",
                    PreparedStatement.RETURN_GENERATED_KEYS)){

                st.setString(1, liq.getName());
                st.setString(2, liq.getBrand());
                st.setInt(3, liq.getNicotineStrength());
                st.setString(4, liq.getFlavour());
                st.setString(5, liq.getPrice().toString());
                st.setString(6,liq.getVolume().toString());
                st.setInt(7, liq.getCount());

                st.executeUpdate();

                ResultSet rs = st.getGeneratedKeys();
                if (rs.next()){
                    liq.setId(rs.getLong(1));
                }
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while creating Liquid", e);
        }
    }

    @Override
    public void deleteLiquid(Long id) throws ServiceFailureException {
        checkDataSource();

        if (id == null) throw new NullPointerException("Attempt to delete null Liquid");

        try (Connection con = ds.getConnection()){
            con.setAutoCommit(false);
            try (PreparedStatement st = con.prepareStatement(
                    "DELETE FROM \"LIQUID\" WHERE \"ID\" = ?")){

                st.setLong(1, id);

                DBUtils.checkUpdatesCount(st.executeUpdate(), id, false);
                con.commit();
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while deleting Liquid", e);
        }
    }

    public void editLiquid(Liquid data) throws ServiceFailureException{
        editLiquid(data.getId(), data);
    }

    @Override
    public void editLiquid(Long toEditId, Liquid data) throws ServiceFailureException {
        checkDataSource();
        if (toEditId == null || data == null) throw new NullPointerException(
                "Null passed as input for the editLiquid method");

        if (data.getCount() == 0){
            deleteLiquid(toEditId);
            return;
        }

        try (Connection con = ds.getConnection()){
            con.setAutoCommit(false);
            try (PreparedStatement st = con.prepareStatement("UPDATE \"LIQUID\" SET " +
                    "\"NAME\" = ?, \"BRAND\" = ?, \"NIC\" = ?, \"FLAVOUR\" = ?, \"PRICE\" = ?" +
                    ", \"VOLUME\" = ?, \"COUNT\" = ?" +
                    "WHERE \"ID\" = ? ")){
                st.setString(1, data.getName());
                st.setString(2, data.getBrand());
                st.setInt(3, data.getNicotineStrength());
                st.setString(4, data.getFlavour());
                st.setString(5, data.getPrice().toString());
                st.setString(6,data.getVolume().toString());
                st.setInt(7, data.getCount());
                st.setLong(8, toEditId);

                DBUtils.checkUpdatesCount(st.executeUpdate(), data, false);
                con.commit();
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while editing Liquid", e);
        }
    }

    public static Set<Liquid> parseDbResult(ResultSet rs){
        Set<Liquid> result = new HashSet<>();
        try{
            while(rs.next()) {
                BigDecimal price = new BigDecimal(rs.getString("PRICE"));
                BigDecimal volume = new BigDecimal(rs.getString("VOLUME"));
                Liquid temp = new Liquid(rs.getString("NAME"), rs.getString("BRAND"),
                        rs.getInt("NIC"), rs.getString("FLAVOUR"), price);
                temp.setVolume(volume);
                temp.setCount(rs.getInt("COUNT"));
                temp.setId(rs.getLong("ID"));

                result.add(temp);
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while getting Liquid values from DB", e);
        }
        return result;
    }

    @Override
    public Liquid getLiquidById(Long id) throws ServiceFailureException {
        checkDataSource();

        if (id == null) return null;

        try (Connection con = ds.getConnection()){
            try (PreparedStatement st = con.prepareStatement("SELECT * FROM \"LIQUID\" WHERE \"ID\" = ?")){

                st.setLong(1, id);

                 try (ResultSet rs = st.executeQuery()){
                     Set<Liquid> temp = parseDbResult(rs);
                     if (temp.size() > 1){
                         throw new ServiceFailureException("Integrity Error, more Liquids with the same ID");
                     }

                     if (temp.isEmpty()) return null;

                     return temp.iterator().next();
                 }
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while getting Liquid from DB", e);
        }
}

    @Override
    public Set<Liquid> getAllLiquids() throws ServiceFailureException {
        checkDataSource();
        Set<Liquid> result;

        try (Connection con = ds.getConnection()){
            try (PreparedStatement st = con.prepareStatement("SELECT * FROM \"LIQUID\"")){
                try (ResultSet rs = st.executeQuery()){
                    result = parseDbResult(rs);
                }
            }
        } catch (SQLException e){
            throw new ServiceFailureException("Error while getting Liquid from DB", e);
        }
        return result;
    }

    /**
     * Sets datasource for this manager
     * @param ds a DataSource for this manager
     */
    public void setDs(DataSource ds) {
        this.ds = ds;
    }
}
