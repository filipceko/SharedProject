package fi.muni.PV168.project.LiquidStore.backend;

import common.LiquidAlreadyInStoreException;
import common.LiquidNotInStore;
import common.ServiceFailureException;

/**
 * @author Filip Čekovský (433588)
 * @version 10.03.2017
 */

public interface StockManager {

    /**
     * Stores Liquid in given store
     * @param store where the liquid is to be stored
     * @param liq to be stored
     * @throws ServiceFailureException if db fails
     * @throws LiquidAlreadyInStoreException if given liquid is already stored somewhere
     */
    void storeLiquid (Store store, Liquid liq) throws ServiceFailureException, LiquidAlreadyInStoreException;

    /**
     * Returns Store where thr liquid is stored
     * @param liq to search for
     * @return Store where the liquid is stored or null if it is not stored
     * @throws ServiceFailureException if db fails
     */
    Store findStoreOfLiquid (Liquid liq) throws  ServiceFailureException;

    /**
     * Returns store where liquid with given id is stored
     * @param id of liquid which is searched for
     * @return Store which contains the liquid or null if does'nt exist
     * @throws ServiceFailureException if db fails
     */
    Store findStoreOfLiquid (Long id) throws ServiceFailureException;

    /**
     * Removes given liquid from given store
     * @param store from which we want to export
     * @param liq that we want to export
     * @throws ServiceFailureException if db fails
     * @throws LiquidNotInStore if given liquid is not in this given store
     */
    void exportLiquidFromStore (Store store, Liquid liq) throws  ServiceFailureException, LiquidNotInStore;
}
