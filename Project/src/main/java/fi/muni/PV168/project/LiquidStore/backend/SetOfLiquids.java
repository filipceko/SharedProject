package fi.muni.PV168.project.LiquidStore.backend;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Filip Čekovský (433588)
 * @version 11.03.2017
 */

public class SetOfLiquids {
    private Set<Liquid> liquids;

    public SetOfLiquids(Set<Liquid> liquids) {
        if (liquids != null){
            this.liquids = liquids;
        }
        else throw new NullPointerException("liquids parameter is null in constructor");
    }

    public SetOfLiquids() {
        liquids = new HashSet<Liquid>();
    }

    public void addLiquid(Liquid liquid){
        liquids.add(liquid);
    }

    public void deleteLiquid(Liquid liquid){
        liquids.remove(liquid);
    }

    public Liquid findLiquidById(Long id){
        for (Liquid item:liquids) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }

    public Set<Liquid> getLiquids() {
        return liquids;
    }
}
