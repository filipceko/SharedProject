package fi.muni.PV168.project.LiquidStore.backend;

import common.ServiceFailureException;

/**
 * @author Filip Čekovský (433588)
 * @version 10.03.2017
 */

public interface StoreManager {
    /**
     * Creates Store in the database
     * @param store to be created in the database
     * @throws ServiceFailureException if db fails
     */
    void createStore(Store store) throws ServiceFailureException;

    /**
     * Deletes Store from the database
     * @param store to be deleted
     * @throws ServiceFailureException if db fails
     */
    void deleteStore(Store store) throws ServiceFailureException;

    /**
     * Edits Store based on a template
     * @param toEdit Store to be Edited
     * @param data template for editing
     * @throws ServiceFailureException if db fails
     */
    void editStore(Store toEdit, Store data) throws ServiceFailureException;

    /**
     *Loads Store from Database
     * @param id of the Store to be loaded
     * @return loaded store from db if found, else null
     * @throws ServiceFailureException if db fails
     */
    Store getStoreById (Long id) throws ServiceFailureException;
}
