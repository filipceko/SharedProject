package common;

/**
 * @author Filip Čekovský (433588)
 * @version 11.03.2017
 */

public class EntityNotFoundException extends RuntimeException{
    public EntityNotFoundException(String msg) {
        super(msg);
    }
}
