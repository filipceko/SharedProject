package common;

/**
 * @author Filip Čekovský (433588)
 * @version 23.03.2017
 */

public class LiquidAlreadyInStoreException extends RuntimeException {
    public LiquidAlreadyInStoreException(String s) {
        super(s);
    }

    public LiquidAlreadyInStoreException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public LiquidAlreadyInStoreException(Throwable throwable) {
        super(throwable);
    }
}
