package common;

/**
 * @author Filip Čekovský (433588)
 * @version 24.03.2017
 */

public class LiquidNotInStore extends RuntimeException {
    public LiquidNotInStore(String s) {
        super(s);
    }

    public LiquidNotInStore(String s, Throwable throwable) {
        super(s, throwable);
    }

    public LiquidNotInStore(Throwable throwable) {
        super(throwable);
    }
}
